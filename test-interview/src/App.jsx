import React from 'react';
import './App.css';
import {Jumbotron} from './Components/Jumbotron/Jumbotron';
import { NiceButton } from './Components/NiceButton/NiceButton';
import { LifeCard } from './Components/Grid/LifeCard';
import {Mode,Grid} from './Components/Grid/Grid';
import {Footer} from './Components/Footer/Footer';
import { Brand } from './Components/Grid/Brand';
// import FontAwesome from 'react-fontawesome';
import {FontAwesomeIcon}  from '@fortawesome/react-fontawesome'
import { faCoffee } from '@fortawesome/fontawesome-free-solid'


function App() {
  const items = [
    {
      title:'ดีคอนโด บลิซ ศรีราชา',
      description:'เตรียมพบคอนโดใหม่ จากแสนสิริ ดีคอนโด บลิซ ให้ชีวิตรีแลกซ์ในแบบรีสอร์ท กับทำเลติด ม.เกษตร ศรีราชา ใกล้นิคมฯ จองวันนี้ ฟรีโอน และส่วนกลาง 2 ปี*',
      price:'1.79',
      nearPlace:{
        bts:'ห้าแยกลาดพร้าว 0.3 กม.',
        mrt:'พหลโยธิน 0.5 กม.'
      },
      img:"https://s3-ap-southeast-1.amazonaws.com/o77site/dcondo-bliss-condo-portrait-810x890.jpg"
    },
    {
      title:'ดีคอนโด บลิซ ศรีราชา',
      description:'เตรียมพบคอนโดใหม่ จากแสนสิริ ดีคอนโด บลิซ ให้ชีวิตรีแลกซ์ในแบบรีสอร์ท กับทำเลติด ม.เกษตร ศรีราชา ใกล้นิคมฯ จองวันนี้ ฟรีโอน และส่วนกลาง 2 ปี*',
      price:'1.79',
      nearPlace:{
        bts:'ห้าแยกลาดพร้าว 0.3 กม.',
        mrt:'พหลโยธิน 0.5 กม.'
      },
      img:"https://s3-ap-southeast-1.amazonaws.com/o77site/dcondo-bliss-condo-portrait-810x890.jpg"
    },
    {
      title:'ดีคอนโด บลิซ ศรีราชา',
      description:'เตรียมพบคอนโดใหม่ จากแสนสิริ ดีคอนโด บลิซ ให้ชีวิตรีแลกซ์ในแบบรีสอร์ท กับทำเลติด ม.เกษตร ศรีราชา ใกล้นิคมฯ จองวันนี้ ฟรีโอน และส่วนกลาง 2 ปี*',
      price:'1.79',
      nearPlace:{
        bts:'ห้าแยกลาดพร้าว 0.3 กม.',
        mrt:'พหลโยธิน 0.5 กม.'
      },
      img:"https://s3-ap-southeast-1.amazonaws.com/o77site/dcondo-bliss-condo-portrait-810x890.jpg"
    },
    {
      title:'ดีคอนโด บลิซ ศรีราชา',
      description:'เตรียมพบคอนโดใหม่ จากแสนสิริ ดีคอนโด บลิซ ให้ชีวิตรีแลกซ์ในแบบรีสอร์ท กับทำเลติด ม.เกษตร ศรีราชา ใกล้นิคมฯ จองวันนี้ ฟรีโอน และส่วนกลาง 2 ปี*',
      price:'1.79',
      nearPlace:{
        bts:'ห้าแยกลาดพร้าว 0.3 กม.',
        mrt:'พหลโยธิน 0.5 กม.'
      },
      img:"https://s3-ap-southeast-1.amazonaws.com/o77site/dcondo-bliss-condo-portrait-810x890.jpg"
    },
    {
      title:'ดีคอนโด บลิซ ศรีราชา',
      description:'เตรียมพบคอนโดใหม่ จากแสนสิริ ดีคอนโด บลิซ ให้ชีวิตรีแลกซ์ในแบบรีสอร์ท กับทำเลติด ม.เกษตร ศรีราชา ใกล้นิคมฯ จองวันนี้ ฟรีโอน และส่วนกลาง 2 ปี*',
      price:'1.79',
      nearPlace:{
        bts:'ห้าแยกลาดพร้าว 0.3 กม.',
        mrt:'พหลโยธิน 0.5 กม.'
      },
      img:"https://s3-ap-southeast-1.amazonaws.com/o77site/dcondo-bliss-condo-portrait-810x890.jpg"
    },
    {
      title:'ดีคอนโด บลิซ ศรีราชา',
      description:'เตรียมพบคอนโดใหม่ จากแสนสิริ ดีคอนโด บลิซ ให้ชีวิตรีแลกซ์ในแบบรีสอร์ท กับทำเลติด ม.เกษตร ศรีราชา ใกล้นิคมฯ จองวันนี้ ฟรีโอน และส่วนกลาง 2 ปี*',
      price:'1.79',
      nearPlace:{
        bts:'ห้าแยกลาดพร้าว 0.3 กม.',
        mrt:'พหลโยธิน 0.5 กม.'
      },
      img:"https://s3-ap-southeast-1.amazonaws.com/o77site/dcondo-bliss-condo-portrait-810x890.jpg"
    },
   
  ]
  // const bdImg = "background-image: url('https://sansiri-com-frontend.s3.ap-southeast-1.amazonaws.com/assets/img/home/sansiri-home-made-for-life-section.jpg?v=1.0.1');";
  // const types=[{img:""},{img:""},{img:""},{img:""},{img:""},{img:""},{img:""}]
  const navLists =['รู้จักแสนสิริ', 'โครงการ' ,'บริการ' ,'ผู้ลงทุนสัมพันธ์','เพื่อชีวิตที่ยั่งยืน' ,'การร่วมทุนทางธุรกิจ'];
  const  madeForLifeItems = [
        {description:'เพราะชีวิตดีๆ...เริ่มต้นที่บ้าน เราจึงมองหาคําตอบของการใช้ชีวิตที่คุณต้องการ เพื่อนำสิ่งนั้นมาประกอบขึ้นเป็น ‘บ้าน’'},
        {description:'เราเชื่อว่า \'รอยยิ้ม\' คือมาตรวัดที่ดีที่สุดของการบริการ เราจึงตั้งใจส่งต่อความสุขด้วยการบริการ และการดูแลที่ครอบคลุมทุกๆ ที่รอบตัวคุณ'},
        {description:'เพราะวัฒนธรรม เป็นหัวใจสำคัญของการเข้าใจ และสร้างพื้นที่เพื่อทุกคน เราจึงสร้างพื้นที่ให้คุณได้ร่วมแบ่งปันมุมมองการใช้ชีวิต'},
        {description:'เพื่อโลกที่น่าอยู่มากขึ้น แสนสิริจึงตั้งใจเนรมิตให้ทุกพื้นที่มีสภาพแวดล้อมที่ดี เพื่อพวกเราทุกคน'},
    ];
  const brandList = [
     { name: 'A'},{ name: 'B'},{ name: 'C'},{ name: 'D'},{ name: 'E'},{ name: 'F'}
  ];
  return (
    <div className="App">

     <div className='logo'>
       <img alt="แสนสิริ - วิกิพีเดีย" class="n3VNCb" src="https://upload.wikimedia.org/wikipedia/commons/b/bb/%E0%B9%81%E0%B8%AA%E0%B8%99%E0%B8%AA%E0%B8%B4%E0%B8%A3%E0%B8%B4-%E0%B8%88%E0%B8%B3%E0%B8%81%E0%B8%B1%E0%B8%94-%E0%B8%A1%E0%B8%AB%E0%B8%B2%E0%B8%8A%E0%B8%99-logo-th.jpg"  />      
    </div>
    <div className='threeDot'>
    <div className='dot' />
    <div className='dot' />
    <div className='dot' />
    </div>
    <Jumbotron navLists={navLists}/>
    <div className='detail'>
    <div className='header'>
      บ้านเดี่ยวแสนสิริ
      </div>
      <div className='info'>
      webpack is a bundler for modules. The main purpose is to bundle JavaScript files for usage in a browser, yet it is also capable of transforming, bundling, or packaging just about any resource or asset.
      </div>
      </div>
      <div className='detachedHouse'>
        <div className='houseHeaderWrapper'>
          <div className='houseTitle'>
            <h1>บ้านเดี่ยว</h1>
            <h3>อะไรก้ไม่รู้มองไม่เห็น</h3>
          </div>
          <NiceButton className='searchbtn'>
            ค้นหาข้อมูลอะไรสักอย่าง
          </NiceButton>
        </div>
       
      <div className='btnWrapper'>
      <div>
      <NiceButton className='iconBtn' >
      <FontAwesomeIcon icon={faCoffee} />
          </NiceButton>
      </div>
      <div className='buttons'>
        <NiceButton className='btn'>
            เลือกอะไรสักอย่าง
          </NiceButton>
          <NiceButton className='btn'>
            เลือกอะไรสักอย่าง
          </NiceButton>
          <NiceButton className='btn'>
            เลือกอะไรสักอย่าง
          </NiceButton>
          <NiceButton className='btn'>
            เลือกอะไรสักอย่าง
          </NiceButton>
        </div>
      </div>

      <Grid mode={Mode.houseCard} items={items}/>
    </div>
    <div className='madeForlifeBg'>
    </div>
    <div className='projects'>
      <h1>แสนสิริ แบรนด์บ้านเดี่ยว</h1>
      <h2>เลือกโครงการบ้านเดี่ยวจาดแบรนด์ที่คุณสนใจได้ที่นี่</h2>
     
    </div>
    <div className='projectsGrid'>
    <Grid itemComponent={Brand} items={brandList} className='brandGrid'classNameItem='brandGridItem'/>
      </div>
    <div className='madeForLife'>
      <span>
      <h1>MADE FOR LIFE</h1>
      <h4>สําหรับแสนสิริ ‘บ้าน’ ไม่ได้เป็นเพียงแค่ที่อยู่อาศัย</h4>
      </span>
      <Grid mode={Mode.lifeCard} items={madeForLifeItems} itemComponent={LifeCard}/>
    </div>
    <Footer />
    </div>
  );
}

export default App;
