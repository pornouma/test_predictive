import React from 'react';
import {FontAwesomeIcon}  from '@fortawesome/react-fontawesome'
import { faCoffee ,faHome} from '@fortawesome/fontawesome-free-solid'
import { faFacebook,faTwitter ,faYoutube} from '@fortawesome/free-brands-svg-icons'

import './style.css';


const info =[
{header:'โครงการน่าลงทุน',lists:['คอนโดพญาไท-ราชเทวี',
'คอนโดทองหล่อ-สุขุมวิท',
'คอนโดรัชดา-พระราม 9',
'คอนโดหมอชิต-พหลฯ',
'คอนโดติด BTS - MRT',
'คอนโดต่างจังหวัด',
'คอนโดน่าลงทุน',
'บ้านปิ่นเกล้า',
'บ้านสายไหม-รังสิต-ปทุมธานี',
'บ้านพระราม 2',
'บ้านราชพฤกษ์',
'บ้านบางนา-ศรีนครินทร์',
'บ้านรามอินทรา-เกษตรนวมินทร์',
'บ้านงามวงศ์วาน-แจ้งวัฒนะ',
'บ้านต่างจังหวัด',]},
{header:'โครงการแสนสิริ',lists:[ 'บ้านเดี่ยว',
'ทาวน์โฮม',
'คอนโดมิเนียม',]},
{header:'แบรนด์บ้านเดี่ยว',lists:[ 'บ้านแสนสิริ',
'นาราสิริ',
'เศรษฐสิริ',
'บุราสิริ',
'สราญสิริ',
'คณาสิริ',
'อณาสิริ',
'ฮาบิเทีย',]},
{header:'แบรนด์ทาวน์โฮม',lists:[ 'ไทเกอร์ เลน',
'ทาวน์ อเวนิว',
'สิริ สแควร์',
'สิริอเวนิว',
'สิริเพลส',
'เมททาวน์',
'วีวิลเลจ',]},
{header:'แบรนด์คอนโดมิเนียม',lists:['98 Wireless',
'เดอะ โมนูเม้นต์',
'KHUN by yoo',
'เวีย',
'เอ็กซ์ที',
'เดอะ ไลน์',
'เอดจ์',
'เฮาส์',
'เดอะ เบส',
'ดีคอนโด',]},

]
export const socialLinks = Object.freeze([
    {
      name: 'Facebook',
      url: 'https://www.facebook.com/',
      icon: <FontAwesomeIcon icon={faFacebook} />,
    },
    {
      name: 'Twitter',
      url: 'https://twitter.com',
      icon: <FontAwesomeIcon icon={faTwitter} />,
    },
    {
      name: 'YouTube',
      url: 'https://www.youtube.com/',
      icon: <FontAwesomeIcon icon={faYoutube} />,
    },
    {
      name: 'LinkedIn',
      url: 'https://www.linkedin.com/',
      icon: <FontAwesomeIcon icon={faCoffee} />,
    },
    {
      name: 'Instagram',
      url: 'https://www.instagram.com/',
      icon: <FontAwesomeIcon icon={faCoffee} />,
    },
    {
      name: 'Home',
      url: 'https://www.home.com//',
      icon: <FontAwesomeIcon icon={faHome} />,
    },
  ]);


export const Footer = (props) => {
    return (
        <div className='FooterWrapper'>
        <div className='topFooter'>
           <div className='group'>
               <h4>โปรโมชัน</h4>
               <h4>โครงการพร้อมอยู่</h4>
               <h4>ความคืบหน้าโครงการ</h4>
               <h4>คําแนะนําในการซื้อ</h4>
               <h4>แบบบ้าน</h4>
           </div>
           {info.map((i) => {
           return (<ul className='group'>
               <h4>{i.header}</h4>
               {i.lists.map((l) => <li><a className='footerLists' href={'/'}>{l}</a></li>)}
           </ul>);

})}
</div>

          
       <div className='bottomFooter'>
       {socialLinks.map((social, idx) => {
    console.log('social: ', social);
          return (
            <ul className='group'>
          <li><a key={idx} title={social.name}  href={social.url}>
            {social.icon}
          </a></li>
          </ul>
        )})}
  </div>
</div>
);
}