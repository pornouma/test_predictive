import React from 'react';
// import styles from './style.scss';
import './houseCard.css';
import {NiceButton} from '../NiceButton/NiceButton';


interface IHouseCard {
    title:string;
    description:string;
    nearPlace:any;
    price:string;
    img:string;
    itemComponent?: any;
}

export const HouseCard: React.FC<IHouseCard> = (props) => {
    const {title,description,nearPlace,price,img} = props;
    console.log('HouseCard: ', title,description,nearPlace,price);
    return (
        <div className='houseCardWrapper'>
        <img className='houseImg' src={img} alt="คอนโดมิเนียม ดีคอนโด บลิซ  ศรีราชา" title="คอนโดมิเนียม ดีคอนโด บลิซ  ศรีราชา"/>
        <div className='detailCard'>
    <h3>{title}</h3>
    <p>
        {description}
        </p>
    <span>เริ่มต้นที่ <strong>{price} </strong> ล้านบาท</span>
    <div className='btsmrt'>
        <div className='bts'>
        <img src="https://www.sansiri.com/uploads/routetype_asset/icon_transport-bts.svg" alt='bts'/>
        <span>{nearPlace.bts}</span>
        </div>
        <div className='mrt'>
        <img src= "https://www.sansiri.com/uploads/routetype_asset/icon_transport-mrt.svg" alt='mrt'/>
        <span>{nearPlace.mrt}</span>
        </div>

    </div>
    </div>
        <NiceButton shape='rectangle' >
            จองออนไลน์
        </NiceButton>
        </div>
    )

}