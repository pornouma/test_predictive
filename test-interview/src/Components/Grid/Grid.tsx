import React from 'react';
import {HouseCard} from './HouseCard';
// import {LifeCard} from './LifeCard';
import classNames from 'classnames';
// import styles from './style.scss';
import './style.css';

export enum Mode {
    lifeCard='LifeCard',
    houseCard='HouseCard'
}
interface IGrid {
    items: Array<any>;
    itemComponent?: any;
    mode?:Mode;
    className?:string;
}

export const Grid: React.FC<IGrid> = (props) => {
    const {items = [], itemComponent = HouseCard,mode=Mode.houseCard,className} = props;
    console.log('items: ', items);
    console.log('mode: ', mode);
    return (
        <section role='grid' className={classNames('grid',className)}>
            {items.map((p,i) => {
            return(<div key={i}>
            {React.createElement(itemComponent, {...p, className: 'missionGridItem'})}
            </div>);
            })}
        </section>

    )

}