import React from 'react';

interface IBrandProps {
    name?:string;
}

export const brandList = [
    'A','B','C','D','E','F'
];

export const Brand: React.FC<IBrandProps> = (props) => {
    return (
        <div className='brand'>

    <div className='brandItem'>
    <img alt="BAAN SANSIRI PATTANAKARN บ้านเดี่ยวระดับซูเปอร์ลักซ์ชัวรี บนทำเลซู"  src="https://pronto-core-cdn.prontomarketing.com/247/wp-content/uploads/sites/2/cache/2020/09/MASTER_LOGO-BAAN_SANSIRI-Black-Indoor/2719901861.jpg" />
    </div>

        </div>
    )

}