import React from 'react';
import './style.css';

interface ILifeCard {
    description?:string;
}


export const LifeCard: React.FC<ILifeCard> = (props) => {
    const {description} = props;
    return (
        <div className='lifeCardWrapper'>
            <img alt="How can we actually create happy societies?" src="https://images.theconversation.com/files/304963/original/file-20191203-66982-1rzdvz4.jpg?ixlib=rb-1.1.0&amp;rect=23%2C15%2C5290%2C3574&amp;q=45&amp;auto=format&amp;w=496&amp;fit=clip" data-noaft="1" ></img>
            <div className='detailLifeCard'>
            <p>
        {description}
        </p>
            </div>
       </div>
    )

}