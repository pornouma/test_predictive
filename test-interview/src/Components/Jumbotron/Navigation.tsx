import React from 'react';

interface INavigationProps {
    navLists?:Array<any>;
}


export const Navigation: React.FC<INavigationProps> = (props) => {
    const {navLists} = props;
    return (
        <div className='navigation'>
            {navLists?.map((n) => {
                return (
                <div className='navItem'>{n}</div>
                )
            })
            }
        </div>
    )

}
