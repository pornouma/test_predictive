import React from 'react';
import {Navigation} from './Navigation'
// import styles from './style.scss';
import './style.css';

interface IJumbotron {
    navLists?:Array<any>;
}

export const Jumbotron: React.FC<IJumbotron> = (props) => {
    const {navLists} = props;
    return (
        <div className='jumbotron'>
        <div className='navWrapper'>
        <Navigation navLists={navLists}/>
        </div>
        <img src="https://onlinebooking.sansiri.com/stocks/project/c650x428/o4/5b/fqnpo45bjl/dcondo-hideaway-condominium-landscape-935x745.jpg" alt="dcondo-hideaway-condominium-landscape-935x745" ></img>
        <img alt="ชมโครงการสุดluxury บ้านแสนสิริ พัฒนาการ - แถลงข่าว | thinkofliving.com"  src="https://cdn-images.prod.thinkofliving.com/wp-content/uploads/1/2018/06/Untitled-4.jpg" />
        </div>
    )

}
