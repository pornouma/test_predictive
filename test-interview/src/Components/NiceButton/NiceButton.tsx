
import React from 'react';
import classNames from 'classnames';
import FontAwesome from 'react-fontawesome';
import './style.css';


interface IButton {
    icon?: string  ;
    children?: React.ReactNode;
    shape?:string;
    className?:string;
}

export const NiceButton: React.FC<IButton> = (props) => {
    const {icon,children,shape,className} = props;
    let iconElement: React.ReactNode;
    let defaultElProps;
    defaultElProps={
        className: classNames(
            'niceButton',
            shape,
            className,
          ),
    }
    if(icon) {
        const iconClassList = classNames('icon', 'icon-right')
        iconElement=<FontAwesome key='icon' name={icon} className={iconClassList} />;

    }
    const element = React.createElement('button', defaultElProps, iconElement, children);
    return element;;
}